use bevy::prelude::*;
//mod hello_world;
mod card_game;

fn main() {
    // hello_world::hello_world(1);
    card_game::main();
    App::new()
        .run();
}
