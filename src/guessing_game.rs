use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn guessing_game(){
    let secret_number = rand::thread_rng().gen_range(1..101);
    // println!("The secret number is: {}", secret_number);

    loop {
        let mut guess = String::new();

        println!("Please Input Your Number");

        // use the standard library's io::stdin function for inputing the variable
        io::stdin()
            // calls the read_line method; & means it's a _reference_; specifies the fact it's mutable
            .read_line(&mut guess)
            .expect("Failed to read line");
        
        // here we compare the guess input
        // shadowing the guess variable instead of making a new one
        // the trim method on a String instance will eliminate any whitespace at the beginning and end
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
    
        println!("You guessed: {}", guess); //debug
        
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;    
            } 
            
        }
    }

}