pub fn hello_world(param: i32) {
    // --------- Hello World --------- //
    let mut hello_world = String::from("Hello World");
    println!("Hello World!");

    // function parameters require a type
    println!("Your parameter is: {}",param);
    // place spaces before value
    println!("{number:>5}", number=1);
    // place char afeter value
    let param_ex = hello_world.push('-');
    println!("-{:?}", param_ex);

    // the println! mactro can have named placeholders
    println!("{subject} {verb} {object}",
             object="the lazy dog",
             subject="the quick brown fox",
             verb="jumps over");

    // integers
    let bit8_variable: u8 = 255;
    // you can also divide the units with an underscore like this 100k
    let bit32_variable: i32 = 100_100;
    let bit64_variable: i64 = 9223372036854775807;
    println!("bit64 = {} bit32 = {} bit8 = {}", bit64_variable, bit32_variable, bit8_variable);
    // you can also use hexadecimal
    let hexadecimal: u32 = 0xff;
    println!("hex = {}", hexadecimal);
    // and convert decimal to hexadecimal when printing
    println!("Base 16 (hexadecimal) repr: {:x}", bit8_variable);

    // constants
    const THREE_HOURS_IN_SECONDS: u32 = 60 * 60 * 3;
    println!("{}", THREE_HOURS_IN_SECONDS);

    // shadowing
    let x = 5;
    let x = x + 1;
    println!("This is x: {}",x);

    // tuplets
    let tuplet: (i32, f64, u8) = (500, 6.4, 1);
    // destructuring the tuplet
    let (x, y, z) = tuplet;
    println!("Tuplet: {},{},{}",x,y,z);
    // accessing a tuplet
    let five_hundred = tuplet.0;
    println!("First tuplet value is: {}",five_hundred);

    // array
    let array: [i32; 5] = [1, 2, 3, 4, 5];
    println!("array ={}",array[1]);
    println!("hello array");

    // expressions
    let expresion = {
        let x = 3;
        x + 1
    };
    println!("expresion = {}",expresion);

    // loop labels
    'counting_up: loop {
        break 'counting_up;
        // we can also do things like multiplying a variable when we break
        // break counter * 2;
    }

    // while loops exist in rust and they are cool
    while five_hundred == 500 {
        break;
    }

    // for loops
    for element in array {
        println!("the value of the element is: {}",element);
    }

    //
}