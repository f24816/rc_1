use std::io;
use rand::{Rng, prelude::ThreadRng};

// the size of a standard deck of cards
const DECK_SIZE : u8 = 52;

pub fn main() {
    // create a hand array to store the cards
    let mut hand: Vec<u8> = vec![];

    // print your cards
    // DEV-NOTE this has to be a mutable reference as well as in the function
    generate_hand(&mut hand);
    println!("Your hand contains {} cards: {:?}",hand.len(), hand);

    loop{
        let mut card_to_play = String::new();
        println!("Chose a card:");
        io::stdin()
            .read_line(&mut card_to_play)
            .expect("Failed to read line");

        let card_to_play: usize = match card_to_play.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };
        println!("You played card: {}", hand[card_to_play-1]);
        break;
    };
}

fn generate_hand(thehand: &mut Vec<u8>) {
    let mut starting_hand_cards: u8 = 6;

    // generate a random number
    let mut rng: ThreadRng = rand::thread_rng();

    // "push" a random card to the hand
    while starting_hand_cards > 0 {
        // add a card between 1 and deck size
        thehand.push(rng.gen_range(1..DECK_SIZE));
        starting_hand_cards -= 1;
    }
}